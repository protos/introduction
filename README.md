## Notes to the Visitors

If you cannot find what you're looking for (for example, for the groups/projects you have the URLs for),
and/or if you are getting "404" page not found errors,
then it's very likely that those groups/projects are private and you need proper permissions.


First, you'll need an account on GitLab.
Create an account and log in, and then try to access the project again.
If you are still getting 404 error,
please ping us with the information as to which project you are trying to access.



